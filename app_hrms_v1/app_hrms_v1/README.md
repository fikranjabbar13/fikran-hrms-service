# README

## STANDART STRUKTUR APPS

Ini adalah struktur dasar pada aplikasi node HRMS.
 - app
    - connection
    - controller
    - routes
      - restapi **for API**
      - socket **for  socket**
    - validator
    - middleware
    - procedure
      - schema **folder untuk config monggose.schema**
    - library **folder penyimpanan lib seperti handle proses upload atau sebagainya.**
    - event **folder ini untuk pengaturan node-schedule jika membutuhkan crontab**
  
- config **folder ini mencakup pengaturan main server, mail, db, redis, path, url dll.**
- storage
- public **folder ini sebagai tempat resource html**
- debug **folder ini sebagai tempat penyimpanan logger debug**
- test **folder ini sebagai pengaturan unit test**
- .env **file environment app**
- index.js

## NEW UPDATE
1.  call script pada apps menggunakan variable GLOBAL dengan nama folder._NAME(arg), memiliki 1 param dengan default null.
    penggunaan require() hanya dipakai untuk call node_module.
    EX :
      - get script controller read : var controller = folder._CONTROLLER('read') or folder._CONTROLLER().read.
      - get script permission on folder middleware : var middle = folder._MIDDLEWARE('permission') or folder._MIDDLEWARE().permission.
    pengaturan ini berada pada file config/app.js::_PATH()

2.  penggunaan file .env sebagai config utama pada apps.

3.  perbaikan penggunaan callback, perbaikan terjadi pada penerapan parameter 1 sebagai error dan parameter 2 sebagai result.
    parameter error akan selalu type String dan parameter result type object ('kecuali pada controller delete, result type boolean').
    EX : 
      - if(err) return callback(err,null); return callback(null, value);

4.  config/app.js, ini berisi config seperti berikut : ip, port, _PATH, _DB_CONFIG, _IMAGE_SERVER, _AUTH_URL, _MAIL

5.  app/library/response.js , ini adalah fungsi build data response. memiliki 3 param : action, form_name dan result.
    return data type object.
    EX : 
      - var method = 'delete', form_name = 'Employee Status'
      - var build_res = folder._LIB('response'); 
      - for error : var obj = build_res(null,null,error); return callback(obj); 
      - for success : var obj = build_res(method, form_name ,result); return callback(obj);

6.  procedure delete with async.waterfall , karena pada proses delete ini harus melakukan beberapa procedure.
    maka saya menggunakan async.waterfall untuk proses.
    silahkan cek app/procedure/delete.js untuk melihat detilnya.

7.  app/middleware/handle_error_method.js , middleware ini diterapkan pada routes/restapi/index.js.
    fungsi utama adalah mengalihkan error bawaan expressjs atau modules lainnya yang berupa html.
    saya convert kedalam bentuk res.json(), agar front-end tetap menerima return data type json.

8.  pada proses delete , procedure checkID dan read menggunakan parameter type string sehingga tidak lagi
    melakukan proses looping saat cek apakah id tersebut ada atau telah digunakan.
    ini dimanipulasi saat data masuk ke validator.
    EX :  SET @q = CONCAT('select status from job_history where job_history.status in(', id_ ,');');
          PREPARE statement1 FROM @q;
          EXECUTE statement1;
          
    ***NOTE :   Procedure MySQL telah menerapkan konsep PREPARE STATEMENT dan param id pada procedure MySQL type varchar, 
                jika procedure MySQL belum menerapkan konsep PREPARE silahkan menggunakan procedure dengan filename deleteSeries (9).

9.  procedure deleteSeries, ini adalah proses menghapus data dengan perulangan.
    memanggil beberapa procedure lainnya yang berkaitan dengan proses ini.

## COMMING SOON
1.  membuat daftar regex pada library untuk validator
2.  membuat singkronasi info max char pada field table untuk menentukan max char pada validator

## How to run this


It will install all the package required for running it and then run the app.

## How to contribute on this repository

Read [CONTRIBUTING.md](CONTRIBUTING.md)