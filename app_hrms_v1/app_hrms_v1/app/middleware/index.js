/** @format */

module.exports = {
    permission: require('./permission'),
    handle_error_method : require('./handle_error_method'),
    upload: require('./upload'),
};
