/** @format */
const buildRes = require('../library').response;

module.exports = (req, res) => {
    /*
     * Handle Error page html
     */
    const msg = buildRes.error.express_requestNotAllowed;
    const resJson = buildRes.build(msg);
    resJson.header.status = 404;

    res.status(404);
    if (req.accepts('html')) {
        res.json(resJson);
        return;
    }
    res.type('txt').send(resJson);
};
