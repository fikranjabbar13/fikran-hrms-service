/** @format */

const libUpload = require('@agustriadji/upload-files');

module.exports = {
    files_upload: libUpload.files_upload,
    multer_err: libUpload.multer_err,
    sendToFileManager: libUpload.sendToFileManager,
    getFileManager: libUpload.getFileManager,
};
