/** @format */
// VARIABLE async sudah di deklar pada root file apps
const async = require('async');
const axios = require('axios');

const APP_CONFIG = require('../../config').app;

const authUrl = APP_CONFIG.AUTH_URL;
const { page } = APP_CONFIG.MAIN_SERVICE;

const mainURL = `${authUrl.url}:${authUrl.port}`;

module.exports = (req, res, next) => {
    let action;

    if (req.method === 'GET') {
        action = 'read';
    } else if (req.method === 'POST') {
        action = 'create';
        if (req.params.id) {
            action = 'update';
        }
    } else if (req.method === 'PUT') {
        action = 'update';
    } else if (req.method === 'DELETE') {
        action = 'destroy';
    }

    async.waterfall(
        [
            (cb) => {
                axios({
                    method: 'POST',
                    url: `${mainURL}/api/authorization?key=${req.query.key}`,
                    headers: { Authorization: `Bearer ${req.query.key}` },
                    data: {
                        page,
                        action,
                    },
                })
                    .then((response) => {
                        if (response.data.header.status !== 200) {
                            return cb(response.data, null);
                        }
                        return cb(null, response.data);
                    })
                    .catch(() => {
                        return cb({
                            header: {
                                message: 'Access Denied.',
                                access: null,
                                status: 500,
                            },
                            data: null,
                        });
                    });
            },
        ],
        (err, data) => {
            /**
             * handle error
             */
            if (err) {
                const { status } = err.header;
                return res.status(status).json(err);
            }
            req.param.access = data;
            return next();
        },
    );
};
