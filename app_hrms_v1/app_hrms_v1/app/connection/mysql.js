/**
 * /* eslint-disable no-console
 *
 * @format
 */

const mysql = require('mysql2');

const cnf = require('../../config').app.DB_CONFIG;

/**
 * create connection to mysql database
 */
const connection = mysql.createConnection({
    host: cnf.host,
    port: cnf.port,
    user: cnf.username,
    password: cnf.password,
    database: cnf.database,
    multipleStatements: true,
});

connection.connect((err) => {
    // eslint-disable-next-line no-console
    if (err) return console.info(`${err} mysql error connection`);

    const connect = connection.config;
    // eslint-disable-next-line no-console
    return console.info(
        `mysql database success connect to host ${connect.host} and table ${connect.database}`,
    );
});

module.exports = connection;
