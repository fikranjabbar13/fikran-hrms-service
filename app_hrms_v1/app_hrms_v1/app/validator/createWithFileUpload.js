/** @format */

const Joi = require('@hapi/joi');
const buildRes = require('../library').response;
const LIBregex = require('../library').regexCollection;

const actionUpload = 'save';

const schema = Joi.object()
    .keys({
        title: Joi.string()
            .regex(LIBregex.aplhaNum)
            .min(3)
            .max(30)
            .required(),
        descript: Joi.string()
            .regex(LIBregex.aplhaNumSymbol)
            .max(250)
            .allow('', null),
        filename: Joi.string().allow('', null),
        path: Joi.string().allow('', null),
    })
    .required();

module.exports = async (data, callback) => {
    try {
        const dataObject = data.body;

        dataObject.body.filename = null;
        dataObject.body.path = null;

        if (typeof dataObject.data === 'object') {
            const files = data.file.data;

            dataObject.body.filename = files.filename;
            dataObject.body.filename = files.path;
        }

        const res = await schema.validateAsync(dataObject.body);

        if (res.filename !== null) {
            res.mimetype = data.file.mimetype;
            res.size = data.file.size;
            res.action = actionUpload;
        }
        return callback(null, res);
    } catch (error) {
        return callback(buildRes.error.validator_inCorrect, null);
    }
};
