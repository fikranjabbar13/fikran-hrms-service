/* eslint-disable no-param-reassign */
/** @format */
const Joi = require('@hapi/joi');
const LIB = require('../library');

const LIBregex = LIB.regexCollection;
const buildRes = LIB.response.error;

const schema = Joi.object()
    .keys({
        title: Joi.string()
            .regex(LIBregex.aplhaNum)
            .min(3)
            .max(30)
            .required(),
        descript: Joi.string()
            .regex(LIBregex.aplhaNumSymbol)
            .min(3)
            .max(250)
            .allow('', null),
        filename: Joi.string().allow('', null),
        path: Joi.string().allow('', null),
        radio: Joi.number()
            .min(1)
            .max(4)
            .allow(null),
    })
    .required();

module.exports = async (data, callback) => {
    try {
        if (typeof data.body.data === 'object') {
            const files = data.file.data;
            data.body.data.filename = files.filename;
            data.body.data.path = files.path;

            data.body.data.radio = Number(data.body.data.radio);
            data.body = data.body.data;
        } else {
            data.body.filename = '';
            data.body.path = '';
        }
        const res = await schema.validateAsync(data.body);

        if (data.body.filename !== '') {
            res.mimetype = data.file.mimetype;
            res.size = data.file.size;
        }

        // Kondisi Jika nochange maka yg dikirm raw data bukan form data
        if (res.radio === 4) {
            res.action = 'save';
        }
        return callback(null, res);
    } catch (error) {
        return callback(buildRes.validator_inCorrect, null);
    }
};
