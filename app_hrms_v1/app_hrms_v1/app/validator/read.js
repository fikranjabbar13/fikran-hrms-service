/** @format */
const Joi = require('@hapi/joi');
const buildRes = require('../library').response.error;
/**
 * please use LIBregex for manage your validation
 * const LIBregex = require('../library').regexCollection;
 * */
const schema = Joi.object().keys({
    offset: Joi.number().allow(null),
    limit: Joi.number().allow(null),
    key: Joi.string(),
});

module.exports = async (req, callback) => {
    try {
        const res = await schema.validateAsync(req.query);
        return callback(null, res);
    } catch (error) {
        return callback(buildRes.validator_inCorrect, null);
    }
};
