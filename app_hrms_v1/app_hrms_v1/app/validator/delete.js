/*
 * input req object
 * output array
 *
 * @format
 */

const Joi = require('@hapi/joi');
const buildRes = require('../library').response;
/**
 * please use LIBregex for manage your validation
 * const LIBregex = require('../library').regexCollection;
 * */

const schema = Joi.object()
    .keys({
        id: Joi.array()
            .items(Joi.number().min(1))
            .required(),
    })
    .required();

module.exports = async (req, callback) => {
    /*
     * MANIPULASI SEBELUM HANDLE JOI
     * data params id akan di ubah ke array
     */

    const data = req.params;
    const dt = data.id.split(',');

    for (let i = 0; i < dt.length; i += 1) {
        dt[i] = Number(dt[i]);
    }

    data.id = dt;

    // hanya terima array dengan value element number and lebih besar dari 0
    try {
        const value = await schema.validateAsync(data);
        return callback(null, value);
    } catch (error) {
        return callback(buildRes.error.validator_inCorrect, null);
    }
};
