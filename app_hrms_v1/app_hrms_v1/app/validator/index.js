/** @format */
module.exports = {
    create: require('./create'),
    delete: require('./delete'),
    read: require('./read'),
    readById: require('./readById'),
    readByFile: require('./readByFile'),
    readOffset: require('./readOffset'),
    update: require('./update'),
    createWithFileUpload: require('./createWithFileUpload'),
    updateWithFileUpload: require('./updateWithFileUpload'),
};
