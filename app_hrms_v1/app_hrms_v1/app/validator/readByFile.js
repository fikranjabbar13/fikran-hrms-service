/*
 * input req object
 * output array
 *
 * @format
 */

const Joi = require('@hapi/joi');
const buildRes = require('../library').response;
const LIBregex = require('../library').regexCollection;
/**
 * please use LIBregex for manage your validation
 * const LIBregex = require('../library').regexCollection;
 * */

const schema = Joi.object()
    .keys({
        filename: Joi.string()
            .regex(LIBregex.fileExt)
            .min(1)
            .required(),
    })
    .required();

module.exports = async (req, callback) => {
    const data = req.params;

    // hanya terima array dengan value element number and lebih besar dari 0

    try {
        const value = await schema.validateAsync(data);
        return callback(null, value);
    } catch (error) {
        return callback(buildRes.error.validator_inCorrect, null);
    }
};
