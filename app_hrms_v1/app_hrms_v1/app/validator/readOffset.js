/** @format */
const Joi = require('@hapi/joi');
const buildRes = require('../library').response;
const LIBregex = require('../library').regexCollection;

const schema = Joi.object()
    .keys({
        id: Joi.number(),
        title: Joi.string()
            .regex(LIBregex.aplhaNum)
            .min(3)
            .max(30),
        offset: Joi.number()
            .integer()
            .positive()
            .allow(0)
            .required(),
        limit: Joi.number()
            .integer()
            .positive()
            .allow(0)
            .required(),
    })
    .required();

module.exports = async (data, callback) => {
    try {
        const value = await schema.validateAsync(data);
        return callback(null, value);
    } catch (error) {
        return callback(buildRes.error.validator_inCorrect, null);
    }
};
