/** @format */

module.exports = {
    firstChar_aplha: '[A-Za-z]',
    firstChar_aplhaNum: '[A-Za-z0-9]',
    aplhaNum: /^[A-Za-z0-9][A-Za-z0-9 ]+$/,
    aplhaNumSymbol: /^[A-Za-z0-9][A-Za-z0-9 -,.'"/!]+$/,
    alpha: /^[A-Za-z][A-Za-z ]+$/,
    fileExt: /([a-zA-Z0-9\s_\\.\-\(\):])+(.doc|.docx|.pdf)$/i,
};
