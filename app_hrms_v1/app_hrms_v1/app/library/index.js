/** @format */

module.exports = {
    response: require('./response'),
    regexCollection: require('./regexCollection'),
};
