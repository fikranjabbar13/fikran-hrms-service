/**
 *
 * @format
 */
const formName = require('../../config').app.MAIN_SERVICE.name;

module.exports = {
    error: {
        validator_inCorrect: 'Your input incorrect, please check again.',
        express_requestNotAllowed: 'Sorry, your request not allowed.',
        multiDelete_inUsed:
            'Delete failed, Some data has been related to other form.',
        multiDelete_notFound:
            'Delete failed, some data was not found during the delete process',
        data_notFound: 'Data not found.',
        data_isEmpty: 'Data is empty.',
        data_isDuplicate: 'The name or title has been already in used.',
        permission_isAccessDenied:
            'Access Denied !, Please contact your administrator to help with your problem.',
    },
    success: (action) => {
        const msg = {
            store: `Store ${formName} Successfully`,
            update: `Update ${formName} Successfully`,
            delete: `Delete ${formName} Successfully`,
            read: `Get data ${formName} Successfully`,
        };
        let message = null;

        if (action === 'store') {
            // STORE
            message = msg.store;
        } else if (action === 'update') {
            // UPDATE
            message = msg.update;
        } else if (action === 'delete') {
            // DELETE
            message = msg.delete;
        } else {
            // READ
            message = msg.read;
        }
        return message;
    },
    build: (error, success, data) => {
        if (error) {
            const dt = {
                header: {
                    message: error,
                    status: 500,
                    access: null,
                },
                data: null,
            };
            return dt;
        }
        const dt = {
            header: {
                message: success,
                status: 200,
                access: null,
            },
            data,
        };
        return dt;
    },
};
