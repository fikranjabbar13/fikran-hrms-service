/** @format */

const controller = require('../../controller');
const middle = require('../../middleware');

const middleUpload = middle.upload;
const upload = middleUpload.files_upload.single('file');

module.exports = (app) => {
    app.route('/api/jobs/jobs-title')
        .post(middle.permission, (req, res) => {
            // eslint-disable-next-line consistent-return
            upload(req, res, (err) => {
                if (err instanceof middleUpload.multer_err) {
                    const msg = {
                        header: {
                            message: err.message,
                            status: 500,
                        },
                        data: { code: err.code },
                    };
                    return res.status(500).json(msg);
                }
                if (err) {
                    // delete err.storageErrors;
                    return res.status(500).json(err);
                }
                controller.create(req, (result) => {
                    // eslint-disable-next-line no-param-reassign
                    result.header.access = req.param.access.data.access;
                    res.status(result.header.status).json(result);
                });
            });
        })
        .get(middle.permission, (req, res) => {
            controller.read(req, (result) => {
                // eslint-disable-next-line no-param-reassign
                result.header.access = req.param.access.data.access;
                res.status(result.header.status).json(result);
            });
        });

    app.route('/api/jobs/jobs-title/:id')
        .post(middle.permission, (req, res) => {
            // eslint-disable-next-line consistent-return
            upload(req, res, (err) => {
                if (err instanceof middleUpload.multer_err) {
                    const msg = {
                        header: {
                            message: err.message,
                            status: 500,
                        },
                        data: { code: err.code },
                    };
                    return res.status(500).json(msg);
                }
                if (err) {
                    // delete err.storageErrors;
                    return res.status(500).json(err);
                }
                controller.update(req, (result) => {
                    // eslint-disable-next-line no-param-reassign
                    result.header.access = req.param.access.data.access;
                    res.status(result.header.status).json(result);
                });
            });
        })
        .delete(middle.permission, (req, res) => {
            controller.delete(req, (result) => {
                // eslint-disable-next-line no-param-reassign
                result.header.access = req.param.access.data.access;
                res.status(result.header.status).json(result);
            });
        });

    app.get('/api/jobs/download/:filename', (req, res) => {
        controller.downloadByFile(req, (result) => {
            // eslint-disable-next-line global-require
            const stream = require('stream');
            try {
                // eslint-disable-next-line no-param-reassign
                // result.header.access = req.param.access.data.access;
                return res.json(result);
            } catch (error) {
                const fileContents = Buffer.from(result.data, 'base64');

                const readStream = new stream.PassThrough();
                readStream.end(fileContents);

                res.set(
                    'Content-disposition',
                    'attachment; filename=nvidia-smi_1587394763194.pdf',
                );
                res.set('Content-Type', 'text/plain');

                return readStream.pipe(res);
            }
        });
    });

    // EXAMPLE ROUTE
    app.route('/yourapi').post(middle.permission, (req, res, next) => {
        upload(req, res, (error) => {
            if (error) {
                if (error instanceof middleUpload.multer_err) {
                    // delete error.storageErrors;
                    const msg = {
                        header: {
                            message: error.message,
                            status: 500,
                        },
                        data: { code: error.code },
                    };
                    return res.status(500).json(msg);
                }
                // delete error.storageErrors;
                return res.status(500).json(error);
            }

            controller.createWithFileUpload(req, (reply) => {
                res.status(reply.header.status);
                res.send(reply);
            });
            return next();
        });
    });
    app.use([middle.handle_error_method]); //  JIKA ROUTE TIDAK DITEMUKAN
};
