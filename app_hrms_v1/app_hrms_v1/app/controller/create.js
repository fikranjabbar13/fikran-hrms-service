/** @format */

const { sendToFileManager } = require('@agustriadji/upload-files');

const method = 'store';
const validator = require('../validator').create;
const procedure = require('../procedure').create;

// param 1 action[get,store,update,delete], param 2 form , param 3 result
const buildRes = require('../library').response;

module.exports = (req, callback) => {
    // eslint-disable-next-line no-undef
    async.waterfall(
        [
            (next) => {
                validator(req, (err, value) => {
                    if (err) return next(err, null);
                    return next(null, value);
                });
            },
            // eslint-disable-next-line consistent-return
            (data, next) => {
                // untuk proses send to image server, kondisi jika selain action save maka langsung lanjut ke procedure;
                if (data.action !== 'save') return next(null, data);

                sendToFileManager(data, (err, value) => {
                    if (err) return next(true, value);
                    return next(null, data);
                });
            },
            (data, next) => {
                procedure(data, (err, value) => {
                    if (err) return next(err, null);
                    return next(null, value);
                });
            },
        ],
        (error, results) => {
            if (error) {
                const resultData = buildRes.build(error);
                return callback(resultData);
            }
            const msg = buildRes.success(method);
            const resultData = buildRes.build(null, msg, results);
            return callback(resultData);
        },
    );
};
