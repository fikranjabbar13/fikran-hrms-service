/**
 * GET ONE DATA
 * @format */

const { getFileManager } = require('@agustriadji/upload-files');

const method = 'get';
const validator = require('../validator').readByFile;
const procedure = require('../procedure').readByFile;

// param 1 action[get,store,update,delete], param 2 form , param 3 result
const buildRes = require('../library').response;

module.exports = (req, callback) => {
    // eslint-disable-next-line no-undef
    async.waterfall(
        [
            (next) => {
                validator(req, (err, value) => {
                    if (err) return next(err, null);
                    return next(null, value);
                });
            },
            (dataValid, next) => {
                procedure(dataValid, (err, value) => {
                    if (err) return next(err, null);
                    return next(null, value);
                });
            },
            // eslint-disable-next-line consistent-return
            (data, next) => {
                getFileManager({ filepath: data.path }, (err, value) => {
                    if (err) return next(buildRes.error.data_notFound, null);

                    return next(null, value);
                });
            },
        ],
        (error, results) => {
            if (error) {
                const resultData = buildRes.build(error);
                return callback(resultData);
            }
            return callback(results);
        },
    );
};
