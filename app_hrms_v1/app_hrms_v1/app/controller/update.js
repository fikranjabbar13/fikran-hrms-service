/** @format */

const async = require('async');
const { sendToFileManager } = require('@agustriadji/upload-files');

const method = 'update';
const validator = require('../validator').update;
const procedure = require('../procedure');
const buildRes = require('../library').response;

const actionProcedure = ['replace', 'delete'];

module.exports = (req, callback) => {
    async.waterfall(
        [
            (next) => {
                validator(req, (err, value) => {
                    if (err) return next(err, null);
                    return next(null, value);
                });
            },
            // eslint-disable-next-line consistent-return
            (dataval, next) => {
                procedure.readById(dataval, (err, value) => {
                    if (err) return next(true, value);
                    return next(null, value);
                });
            },
            // eslint-disable-next-line consistent-return
            (data, next) => {
                // untuk proses send to image server, kondisi jika selain action save maka langsung lanjut ke procedure;
                if (actionProcedure.indexOf(data.action) === -1)
                    return next(null, data);

                sendToFileManager(data, (err, value) => {
                    if (err) return next(true, value);
                    return next(null, data);
                });
            },
            (dataVal, next) => {
                procedure.update(dataVal, (err, value) => {
                    if (err) return next(err, null);
                    return next(null, value);
                });
            },
        ],
        (error, results) => {
            if (error) {
                const resultData = buildRes.build(error);
                return callback(resultData);
            }
            const msg = buildRes.success(method);
            const resultData = buildRes.build(null, msg, results);
            return callback(resultData);
        },
    );
};
