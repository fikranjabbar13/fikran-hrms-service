/** @format */

let self;
async = require('async');

module.exports = {
    create: require('./create'),
    delete: require('./delete'),
    read: require('./read'),
    readOffset: require('./readOffset'),
    update: require('./update'),
    readById: require('./readById'),
    createWithFileUpload: require('./createWithFIleUpload'),
    downloadByFile: require('./downloadByFile'),
};
