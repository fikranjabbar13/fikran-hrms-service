/** @format */

const method = 'store';
const validator = require('../validator').createWithFileUpload;

// param 1 action[get,store,update,delete], param 2 form , param 3 result
const buildRes = require('../library').response;
const middleUpload = require('../middleware').upload;

module.exports = (req, callback) => {
    // eslint-disable-next-line no-undef
    async.waterfall(
        [
            (next) => {
                validator(req, (err, value) => {
                    if (err) return next(err, null);
                    return next(null, value);
                });
            },
            (datas, next) => {
                middleUpload.sendToFileManager(datas, (err, value) => {
                    if (err) return next(true, value);

                    return next(null, datas);
                });
            },
            /*
             * (next) => {
             * // YOUR STATEMENT PROCEDURE
             *},0110
             */
        ],
        (error, results) => {
            if (error) {
                const resultData = buildRes.build(error);
                return callback(resultData);
            }
            const msg = buildRes.success(method);
            const resultData = buildRes.build(null, msg, results);
            return callback(resultData);
        },
    );
};
