/**
 * GET ALL DATA
 * Tanpa Validator, karena tidak membutuhkan params ataupun query pada url
 * @format */

const method = 'get';
const procedure = require('../procedure').read;

// param 1 action[get,store,update,delete], param 2 form , param 3 result
const buildRes = require('../library').response;

module.exports = (req, callback) => {
    // eslint-disable-next-line no-undef
    async.waterfall(
        [
            (next) => {
                procedure((err, value) => {
                    if (err) return next(err, null);
                    return next(null, value);
                });
            },
        ],
        (error, results) => {
            if (error) {
                const resultData = buildRes.build(error);
                return callback(resultData);
            }
            const msg = buildRes.success(method);
            const resultData = buildRes.build(null, msg, results);
            return callback(resultData);
        },
    );
};
