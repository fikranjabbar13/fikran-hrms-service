/** @format */

const async = require('async');

const method = 'delete';
const validator = require('../validator/delete');
const procedure = require('../procedure/delete');

// procedure require('../procedure/deleteSeries'), // FOR LOOP DELETE

const buidRes = require('../library').response;

module.exports = (req, callback) => {
    async.waterfall(
        [
            (next) => {
                validator(req, (err, value) => {
                    if (err) return next(err, null);
                    return next(null, value);
                });
            },
            (dataValid, next) => {
                procedure(dataValid, (err, value) => {
                    if (err) return next(err, null);
                    return next(null, value);
                });
            },
        ],
        (error, results) => {
            if (error) {
                const resultData = buidRes.build(error);
                return callback(resultData);
            }
            const msg = buidRes.success(method);
            const resultData = buidRes.build(null, msg, results);
            return callback(resultData);
        },
    );
};
