/** @format */

const async = require('async');
const database = require('../connection').mysql;
const { checkIdSeries, checkRelationSeries } = require('./');

module.exports = (data, callback) => {
    // HANDLE MULTIPLE DELETE, param data must be ARRAY
    async.waterfall(
        [
            (next) => {
                // HANDLE CHECK ID ON TABLE
                checkIdSeries(data.id, (err) => {
                    if (err) return next(err);
                    return next(null);
                });
            },
            (next) => {
                // HANDLE CHECK ID HAS BEEN REALTED TO OTHER TABLE
                checkRelationSeries(data.id, (err) => {
                    if (err) return next(err);
                    return next(null);
                });
            },
            (next) => {
                // AUTO DELETE WITH CHECKING AGAIN.
                async.eachSeries(
                    data.id,
                    (str, cb) => {
                        setImmediate(() => {
                            database.query(
                                'CALL delete_department(?)',
                                [str],
                                (err, result) => {
                                    try {
                                        if (
                                            result[0][0].status !== 'success' &&
                                            result[0][0].status !== undefined
                                        ) {
                                            return cb(result[0][0].message);
                                        }
                                    } catch (error) {
                                        if (err) return cb(err.sqlMessage);
                                    }

                                    return cb(null);
                                },
                            );
                        });
                    },
                    (err) => {
                        if (err) return next(err, null);
                        return next(null, null);
                    },
                );
            },
        ],
        (err, result) => {
            if (err) return callback(err, null);
            return callback(null, result);
        },
    );
};
