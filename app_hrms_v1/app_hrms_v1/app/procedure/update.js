/** @format */
const database = require('../connection').mysql;
// const buildRes = require('../library/response');

module.exports = (data, callback) => {
    if (data.action === 'delete') {
        // eslint-disable-next-line no-param-reassign
        data.filename = null;
        // eslint-disable-next-line no-param-reassign
        data.path = null;
    } else if (data.action === 'replace') {
        if (!data.filename) {
            // eslint-disable-next-line no-param-reassign
            data.filename = data.currentFilename;
        }
    }

    database.query(
        'CALL update_job(?, ?, ?, ?, ?)',
        [data.title, data.descript, data.filename, data.path, data.id],
        (err, result) => {
            try {
                if (err) return callback(err.sqlMessage, null);
                if (
                    result[0][0].status !== 'success' &&
                    result[0][0].status !== undefined
                ) {
                    return callback(result[0][0].message, null);
                }
            } catch (error) {
                if (err) return callback(err.sqlMessage, null);
            }
            return callback(null, null);
        },
    );
};
