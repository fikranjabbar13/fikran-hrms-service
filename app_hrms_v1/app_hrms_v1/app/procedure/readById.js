/*
 * GET BY ID DENGAN METHOD PREPARE
 * PARAMS INPUT data type array
 * PROCEDURE MySQL id type VARCHAR
 * RETURN PROCEDURE MySQL IS OBJECT { status('error','success'), message }
 *
 * RETURN callback(error, result)
 * PARAMS OUPUT error type string
 * PARAMS OUPUT result type boolean
 * @format
 *
 * */

const database = require('../connection').mysql;
const buildRes = require('../library').response;

module.exports = (data, callback) => {
    // BASED QUERY WITH PREPARE AND EXECUTE STATEMENT.
    // CONVERT ARRAY TO STRING
    const datas = data;

    database.query(`CALL view_jobbyid_series(?)`, [datas.id], (err, result) => {
        try {
            if (err) return callback(buildRes.error.data_notFound, null);
            if (result[0].length === 0)
                return callback(buildRes.error.data_notFound, null);

            if (data.action === 'replace' || data.action === 'delete') {
                datas.currentFilename = datas.filename;
                if (result[0][0].filename)
                    datas.filename = result[0][0].filename;
            } else {
                datas.filename = result[0][0].filename;
                datas.path = result[0][0].path;
            }

            return callback(null, datas);
        } catch (error) {
            return callback(buildRes.error.data_notFound, null);
        }
    });
};
