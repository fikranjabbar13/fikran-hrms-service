/*
 * CHECK ID DENGAN METHOD SERIES
 * PARAMS INPUT data type array
 * PROCEDURE MySQL id type INT
 * RETURN PROCEDURE MySQL IS OBJECT { status('error','success'), message }
 *
 * RETURN callback(error, result)
 * PARAMS OUPUT error type string
 * PARAMS OUPUT result type boolean
 * @format
 *
 * */

const async = require('async');
const database = require('../connection').mysql;
const buildRes = require('../library').response;

module.exports = (data, callback) => {
    async.eachSeries(
        data.id,
        (str, cb) => {
            setImmediate(() => {
                database.query(
                    `CALL view_department_by_id(?)`,
                    [str],
                    // eslint-disable-next-line consistent-return
                    (err, result) => {
                        try {
                            if (err) return cb(buildRes.error.data_not_found);
                            if (result[0][0].status !== 'success') {
                                return cb(result[0][0].message);
                            }

                            return cb(null);
                        } catch (error) {
                            return cb(error);
                        }
                    },
                );
            });
        },
        (err) => {
            if (err) return callback(err);
            return callback(null);
        },
    );
};
