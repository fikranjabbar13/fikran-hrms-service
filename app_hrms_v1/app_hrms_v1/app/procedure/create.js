/** @format */
const database = require('../connection').mysql;
// const buildRes = require('../library/response');

module.exports = (data, callback) => {
    database.query(
        'CALL insert_job(?, ?, ?, ?)',
        [data.title, data.descript, data.filename, data.path],
        (err, result) => {
            try {
                if (
                    result[0][0].status !== 'success' &&
                    result[0][0].status !== undefined
                ) {
                    return callback(result[0][0].message, null);
                }
            } catch (error) {
                if (err) return callback(err.sqlMessage, null);
            }
            return callback(null, result[0][0]);
        },
    );
};
