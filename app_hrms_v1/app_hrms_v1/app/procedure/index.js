/** 
 * @format */

module.exports = {
    checkId: require('./checkId'),
    checkIdSeries: require('./checkIdSeries'),
    
    checkRelation: require('./checkRelation'),
    checkRelationSeries: require('./checkRelationSeries'),
    
    create: require('./create'),
    
    delete: require('./delete'),
    deleteSeries: require('./deleteSeries'),
    
    read: require('./read'),
    readById: require('./readById'),
    readByFile: require('./readByFile'),
    readOffset: require('./readOffset'),
    
    update: require('./update'),
};
