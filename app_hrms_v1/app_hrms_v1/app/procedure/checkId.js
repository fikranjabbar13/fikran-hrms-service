/*
 * CHECK ID DENGAN METHOD PREPARE
 * PARAMS INPUT data type array
 * PROCEDURE MySQL id type VARCHAR
 * RETURN PROCEDURE MySQL IS ARRAY RECORD
 *
 * RETURN callback(error, result)
 * PARAMS OUPUT error type string
 * PARAMS OUPUT result type boolean
 * @format
 *
 * */

const database = require('../connection').mysql;
const buildRes = require('../library').response;

module.exports = (data, cb) => {
    // BASED QUERY WITH PREPARE AND EXECUTE STATEMENT.
    // CONVERT ARRAY TO STRING
    const str = data.id.join(',');

    database.query(
        `CALL view_department_by_id_prepare(?)`,
        [str],
        // eslint-disable-next-line consistent-return
        (err, result) => {
            try {
                if (err) return cb(buildRes.error.multiDelete_inUsed, null);
                if (result[0].length !== data.id.length)
                    return cb(buildRes.error.multiDelete_notFound, null);

                return cb(null, true);
            } catch (error) {
                return cb(error, null);
            }
        },
    );
};
