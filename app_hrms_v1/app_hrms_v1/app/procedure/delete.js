/** @format */

const async = require('async');
const database = require('../connection').mysql;

const checkId = require('./checkId');
const checkRelation = require('./checkRelation');

module.exports = (data, callback) => {
    // HANDLE MULTIPLE DELETE, param data must be ARRAY
    async.waterfall(
        [
            (next) => {
                // HANDLE CHECK ID ON TABLE
                checkId(data, (err) => {
                    if (err) return next(err);
                    return next(null);
                });
            },
            (next) => {
                // HANDLE CHECK ID HAS BEEN REALTED TO OTHER TABLE
                checkRelation(data, (err) => {
                    if (err) return next(err);
                    return next(null);
                });
            },
            (next) => {
                // AUTO DELETE WITH CHECKING AGAIN.
                database.query(
                    'CALL delete_department_prepare(?)',
                    [data.id],
                    (err, result) => {
                        if (err) return next(err.sqlMessage, null);
                        if (result[0][0].status === 'success') {
                            return next(null, null);
                        }
                        return next(null, result);
                    },
                );
            },
        ],
        (err, result) => {
            if (err) return callback(err, null);
            return callback(null, result);
        },
    );
};
