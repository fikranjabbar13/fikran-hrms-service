/*
 * GET BY ID DENGAN METHOD PREPARE
 * PARAMS INPUT data type array
 * PROCEDURE MySQL id type VARCHAR
 * RETURN PROCEDURE MySQL IS OBJECT { status('error','success'), message }
 *
 * RETURN callback(error, result)
 * PARAMS OUPUT error type string
 * PARAMS OUPUT result type boolean
 * @format
 *
 * */

const database = require('../connection').mysql;
const buildRes = require('../library').response;

module.exports = (data, callback) => {
    // BASED QUERY WITH PREPARE AND EXECUTE STATEMENT.
    // CONVERT ARRAY TO STRING
    const datas = data;

    database.query(
        `CALL view_job_by_file_series(?)`,
        [datas.filename],
        (err, result) => {
            try {
                if (err) return callback(buildRes.error.data_notFound, null);
                if (
                    result[0].length === 0 ||
                    !result[0][0].filename ||
                    !result[0][0].path
                )
                    return callback(buildRes.error.data_notFound, null);

                datas.filename = result[0][0].filename;
                datas.path = `${result[0][0].path}${result[0][0].filename}`;

                if (result[0][0].path) {
                    const split = result[0][0].path.split('/');
                    const checkLast = split[split.length - 1];

                    if (checkLast.length > 1)
                        datas.path = `${result[0][0].path}/${result[0][0].filename}`;
                }

                return callback(null, datas);
            } catch (error) {
                return callback(buildRes.error.data_notFound, null);
            }
        },
    );
};
