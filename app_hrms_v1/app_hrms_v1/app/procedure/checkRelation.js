/*
 * CHECK RELATION DENGAN METHOD PREPARE STATEMENT
 * PARAMS INPUT data type array
 * PROCEDURE MySQL id type VARCHAR
 * RETURN PROCEDURE MySQL IS OBJECT { status('error','success'), message }
 *
 * RETURN callback(error, result)
 * PARAMS OUPUT error type string
 * PARAMS OUPUT result type boolean
 * @format
 *
 * */

const database = require('../connection').mysql;
const buildRes = require('../library').response;

module.exports = (data, callback) => {
    // BASED QUERY WITH PREPARE AND EXECUTE STATEMENT.
    // CONVERT ARRAY TO STRING

    const str = data.id.join(',');

    database.query(
        `CALL view_department_relation_prepare(?)`,
        [str],
        // eslint-disable-next-line consistent-return
        (err, result) => {
            try {
                if (result[0][0].status !== 'success')
                    return callback(result[0][0].message, null);

                return callback(null, true);
            } catch (error) {
                if (err) return callback(buildRes.error.delete_inused, null);
            }
        },
    );
};
