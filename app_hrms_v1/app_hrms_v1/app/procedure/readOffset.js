/*
 * GET ALL DATA DENGAN METHOD SERIES
 *
 * RETURN PROCEDURE MySQL IS ARRAY RECORD
 *
 * RETURN callback(error, result)
 * PARAMS OUPUT error type string
 * PARAMS OUPUT result type ARRAY
 * @format
 *
 * */

const database = require('../connection').mysql;
const buildRes = require('../library').response;

module.exports = (data, callback) => {
    database.query(
        `CALL view_department(?,?)`,
        [data.offset, data.limit],
        (err, result) => {
            if (err) return callback(err.sqlMessage, null);
            if (result[0].length > 0) {
                if (result[0][0].status === undefined) {
                    return callback(null, result[0]);
                }
                if (result[0][0].status !== 'success') {
                    return callback(result[0][0].message, null);
                }
            }
            return callback(buildRes.error.data_empty, null);
        },
    );
};
