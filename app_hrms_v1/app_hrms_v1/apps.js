/** @format */

const express = require('express');
const cors = require('cors');
const http = require('http');
const fs = require('fs');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');

const app = express();
const server = http.createServer(app);
const packagejs = require('./package');

const service = packagejs.name;
const { version } = packagejs;

const APP_CONFIG = require('./config/app');

const appPort = APP_CONFIG.SERVE_APP.port;
const appUrl = APP_CONFIG.SERVE_APP.url;

app.use(logger('dev'));
app.use(
    logger('common', {
        stream: fs.createWriteStream(path.join(__dirname, 'log/access.log'), {
            flags: 'a',
        }),
    }),
);

app.use(cors());
app.use(bodyParser.json({ limit: '200mb' }));
// app.use(bodyParser.urlencoded({ extended: false, limit: '200mb' }));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Secret',
    );
    res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Content-Type', 'application/json');
    res.setHeader(
        'Access-Control-Allow-Headers',
        'X-Requested-With,content-type,Authorization',
    );
    next();
});

const restapi = require('./app/routes/restapi');

restapi(app);

server.listen(appPort, appUrl, () => {
    // eslint-disable-next-line no-console
    console.info(
        `${service} version ${version} listening on ${appUrl}:${appPort}`,
    );
});
