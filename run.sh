###############################################################
#####  		RUN ALL SERVICE SERVER MASTER TRADEPRO  				#####
###############################################################

# PULL FROM BITBUCKET #
sh gitpull.sh


# RUN SERVICE GATEWAY TRADEPRO #
# port 80 & 9876 
# pm2 start ./gateway-tradepro/gateway-server.js


# RUN SERVICES SERVER HRMS #

# port '4002'
pm2 start ./crud_node__jobtitle/crudjobtitle.js

# port '4006'
pm2 start ./crud_node__language/crudlanguage.js

# port '4010'
pm2 start ./crud_node__terminationreasons/crudterminationreasons.js

    							

# port 16000
#pm2 start ./jwtEngine.js 

# port 16000
#pm2 start ./hrms.js  																										

pm2 list

pm2 log													
